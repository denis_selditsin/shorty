<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shorty</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/link.js') }}"></script>
</head>
<body>
<div class="wrapper">
    <div class="form mb-3">
        <div class="form-group">
            <label for="input-link">Адрес ссылки, начиная с <b>http(s)://</b>:</label>
            <input type="text" class="form-control" id="input-link" placeholder="https://">
        </div>
        <button type="button" class="btn btn-primary w-100">Сократить</button>
    </div>

    <div class="alert alert-success text-center" role="alert" style="display: none">
        <span class="link"></span>
    </div>

    <script>
        $('button').on('click', () => {
            let link_element = $('#input-link');
            if (link_element.val().indexOf('://') !== -1) {
                $.ajax({
                    url: "{{ request()->root() }}/api/create",
                    method: 'post',
                    dataType: 'json',
                    data: {
                        link: link_element.val()
                    },
                    success: (response) => {
                        if (response.id) {
                            $('.alert').show()
                            $('.link').text('{{ request()->root() }}/' + response.token)
                            $('#input-link').attr('style', '')
                        }
                    }
                })
            } else {
                link_element.attr('style', 'border: 2px solid #ff000061')
            }
        })
    </script>
</div>
</body>
</html>
