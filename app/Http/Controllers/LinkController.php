<?php

namespace App\Http\Controllers;

use App\Models\Link;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Illuminate\View\View;

class LinkController extends Controller
{

    public function show()
    {
        return view('index');
    }

    public function create()
    {
        if (isset($_POST['link']) && strpos($_POST['link'], '://') !== false) {
            do {
                $token = Str::random(6);
            } while ($token == Link::select('link')->where('token', '=', $token));
            $response = Link::create([
                'link' => $_POST['link'],
                'token' => $token
            ]);
        } else {
            $response = ["error" => "validation error"];
        }

        return response()->json($response, 201);
    }

    public function redirect($token)
    {
        $redirect_link = Link::select('link')->where('token', '=', $token)->first();
        return redirect()->away($redirect_link['link']);
    }
}
